/*
 * =============================================================================
 *   Copyright (C) 2010-2017 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * =============================================================================
 */

package net.sf.okapi.filters.openxml;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import java.io.Reader;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import static net.sf.okapi.filters.openxml.RunProperties.copiedRunProperties;

/**
 * Provides presentation notes style definitions.
 *
 * @author ccudennec
 * @since 06.09.2017
 */
class PresentationNotesStyleDefinitions implements StyleDefinitions {
    private final ConditionalParameters conditionalParameters;
    private final XMLInputFactory inputFactory;
    private final XMLEventFactory eventFactory;

    private RunProperties extraDocumentDefaultRunProperties;
    private PresentationNotesStyleDefinition documentDefaults;
    private Map<String, PresentationNotesStyleDefinition> stylesByStyleIds;

    PresentationNotesStyleDefinitions(
        final ConditionalParameters conditionalParameters,
        final XMLInputFactory inputFactory,
        final XMLEventFactory eventFactory
    ) {
        this.conditionalParameters = conditionalParameters;
        this.inputFactory = inputFactory;
        this.eventFactory = eventFactory;
    }

    @Override
    public void readWith(final Reader reader) throws XMLStreamException {
        this.extraDocumentDefaultRunProperties = extraDocumentDefaultRunProperties();
        this.stylesByStyleIds = new LinkedHashMap<>();

        final PresentationNotesStyleDefinitionsReader styleDefinitionsReader = new PresentationNotesStyleDefinitionsReader(
            this.conditionalParameters,
            this.inputFactory,
            this.eventFactory,
            reader
        );
        this.documentDefaults = styleDefinitionsReader.readDocumentDefaults();
        while (styleDefinitionsReader.hasNextParagraphLevel()) {
            place(styleDefinitionsReader.readNextParagraphLevel());
        }
    }

    private RunProperties extraDocumentDefaultRunProperties() {
        return new RunProperties.Default(
            eventFactory,
            null,
            null,
            Collections.singletonList(
                RunPropertyFactory.createRunProperty(
                    new QName(null, "baseline"),
                    "0"
                )
            )
        );
    }

    private void place(final PresentationNotesStyleDefinition styleDefinition) {
        this.stylesByStyleIds.put(styleDefinition.id(), styleDefinition);
    }

    @Override
    public void place(final String parentId, final RunProperties runProperties) {
    }

    @Override
    public String placedId() {
        return null;
    }

    /**
     * Gets the combined run properties by applying the styles in the following order:
     *
     * <ul>
     * <li>extra document default styles (see {@link #extraDocumentDefaultRunProperties}</li>
     * <li>paragraph level styles from master</li>
     * <li>direct styles {@link RunProperties}</li>
     * </ul>
     *
     * @param paragraphLevelId The paragraph level ID
     * @param runStyle         <i>Unused</i>
     * @param runProperties    The run properties
     *
     * @return The combined run properties
     */
    @Override
    public RunProperties combinedRunProperties(String paragraphLevelId, String runStyle, RunProperties runProperties) {

        RunProperties combinedRunProperties = copiedRunProperties(extraDocumentDefaultRunProperties);

        combinedRunProperties.combineDistinct(copiedRunProperties(getParagraphLevelProperties(paragraphLevelId)), TraversalStage.DIRECT);

        combinedRunProperties = combinedRunProperties.combineDistinct(copiedRunProperties(runProperties), TraversalStage.DIRECT);

        return combinedRunProperties;
    }

    private RunProperties getParagraphLevelProperties(String paragraphLevelId) {
        if (null == paragraphLevelId) {
            return this.documentDefaults.runProperties();
        }
        final int id = Integer.valueOf(paragraphLevelId) + 1;
        return stylesByStyleIds.get(String.valueOf(id)).runProperties();
    }

    @Override
    public Markup toMarkup() {
        throw new UnsupportedOperationException();
    }
}
