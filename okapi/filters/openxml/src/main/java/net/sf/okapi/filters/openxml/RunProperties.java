/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import java.util.stream.Collectors;

import static net.sf.okapi.filters.openxml.XMLEventHelpers.startElementEquals;

/**
 * Representation of the parsed properties of a text run.  Immutable.
 */
abstract class RunProperties implements MarkupComponent {
	static final String RPR = "rPr";
	static final String DEF_RPR = "defRPr";

	abstract List<RunProperty> getProperties();

	public int count() {
		return getProperties().size();
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (o == null || !(o instanceof RunProperties)) return false;
		return equalsProperties((RunProperties) o);
	}

	protected abstract boolean equalsProperties(RunProperties rp);

	@Override
	public abstract int hashCode();

	@Override
	public abstract List<XMLEvent> getEvents();

	public abstract RunProperties combineDistinct(RunProperties otherProperties, StyleDefinitions.TraversalStage traversalStage);

	/**
	 * Checks whether a property is present.
	 *
	 * @param name The name of the property
	 * @return {@code true} if the property is present
	 *         {@code false} otherwise
	 */
	boolean isPresent(final String name) {
		for (final RunProperty property : getProperties()) {
			if (property.getName().getLocalPart().equals(name)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Gets the run style property.
	 *
	 * @return The run style proprety
	 */
	RunProperty.RunStyleProperty getRunStyleProperty() {
		for (RunProperty property : getProperties()) {
			if (property instanceof RunProperty.RunStyleProperty) {
				return (RunProperty.RunStyleProperty) property;
			}
		}

		return null;
	}

	/**
	 * Gets the run's highlight property.
	 *
	 * @return The run's hightlight proprety
	 */
	RunProperty.RunHighlightProperty getHighlightProperty() {
		for (RunProperty property : getProperties()) {
			if (property instanceof RunProperty.RunHighlightProperty) {
				return (RunProperty.RunHighlightProperty) property;
			}
		}

		return null;
	}

	/**
	 * Gets the run's color property.
	 *
	 * @return The run's color proprety
	 */
	RunProperty.RunColorProperty getRunColorProperty() {
		for (RunProperty property : getProperties()) {
			if (property instanceof RunProperty.RunColorProperty) {
				return (RunProperty.RunColorProperty) property;
			}
		}

		return null;
	}

	/**
	 * Gets mergeable run properties.
	 *
	 * @return Mergeable run properties
	 */
	List<RunProperty> getMergeableRunProperties() {
		List<RunProperty> properties = new ArrayList<>(getProperties().size());

		for (RunProperty property : getProperties()) {
			if (property instanceof MergeableRunProperty) {
				properties.add(property);
			}
		}

		return properties;
	}

	List<RunProperty> attributeRunProperties() {
		return getProperties()
			.stream()
			.filter(runProperty -> runProperty instanceof RunProperty.AttributeRunProperty)
			.collect(Collectors.toList());
	}

	/**
	 * Refines run properties by updating exiting or adding new
	 * and aligning with their schema definition.
	 *
	 * @param commonRunProperties The run properties to check against
	 */
	void refine(final List<RunProperty> commonRunProperties) {
		for (final RunProperty commonRunProperty : commonRunProperties) {
			updateOrAdd(commonRunProperty);
		}
	}

	private void updateOrAdd(final RunProperty commonRunProperty) {
		final ListIterator<RunProperty> iterator = getProperties().listIterator();
		while (iterator.hasNext()) {
			final RunProperty currentRunProperty = iterator.next();
			if (currentRunProperty.getName().equals(commonRunProperty.getName())) {
				iterator.set(commonRunProperty);
				return;
			}
		}
		iterator.add(commonRunProperty);
	}

	/**
	 * Aligns run properties with the provided schema definition.
	 *
	 * @param schemaDefinition    The schema definition to align with
	 */
	void alignWith(final SchemaDefinition.Component schemaDefinition) {
		if (getProperties().isEmpty()) {
			// there is nothing to align with
			return;
		}
		final List<RunProperty> copiedProperties = new ArrayList<>(getProperties());
		final List<RunProperty> attributeRunProperties = attributeRunProperties();
		getProperties().retainAll(attributeRunProperties);
		copiedProperties.removeAll(attributeRunProperties);

		final ListIterator<SchemaDefinition.Component> iterator = schemaDefinition.listIterator();
		while (iterator.hasNext() || !copiedProperties.isEmpty()) {
			final SchemaDefinition.Component component = iterator.next();
			switch (component.composition()) {
				case CHOICE:
				case SEQUENCE:
				case ALL:
					findAndAppendMany(copiedProperties, component);
					break;
				case NONE:
					findAndAppendOne(copiedProperties, component);
			}
		}
	}

	private void findAndAppendMany(final List<RunProperty> copiedProperties, final SchemaDefinition.Component component) {
		final Iterator<SchemaDefinition.Component> componentsIterator = component.listIterator();
		while (componentsIterator.hasNext()) {
			final SchemaDefinition.Component innerComponent = componentsIterator.next();
			switch (innerComponent.composition()) {
				case CHOICE:
				case SEQUENCE:
				case ALL:
					findAndAppendMany(copiedProperties, innerComponent);
					break;
				case NONE:
					findAndAppendOne(copiedProperties, innerComponent);
			}
		}
	}

	private void findAndAppendOne(final List<RunProperty> copiedProperties, final SchemaDefinition.Component component) {
		final Iterator<RunProperty> copiedPropertiesIterator = copiedProperties.iterator();
		while (copiedPropertiesIterator.hasNext()) {
			final RunProperty runProperty = copiedPropertiesIterator.next();
			if (runProperty.getName().equals(component.name())) {
				getProperties().add(runProperty);
				copiedPropertiesIterator.remove();
				return;
			}
		}
	}

	/**
	 * Removes run property by name.
	 *
	 * @param runProperty The run property
	 */
	void remove(final RunProperty runProperty) {
		final Iterator<RunProperty> propertiesIterator = getProperties().iterator();
		while (propertiesIterator.hasNext()) {
			final RunProperty currentRunProperty = propertiesIterator.next();
			if (currentRunProperty.getName().equals(runProperty.getName())) {
				propertiesIterator.remove();
				return;
			}
		}
	}

	/**
	 * Create a copy of an exiting RunProperties object, optionally stripping the 
	 * <w:vertAlign> or <w:rStyle> or toggle property.
	 *
	 * @param existingProperties Existing properties
	 * @param stripVerticalAlign Strip vertical align property flag
	 * @param stripRunStyle      Strip run style property flag
	 * @param stripToggle        Strip toggle property flag
	 *
	 * @return A possibly stripped copy of run properties
	 */
	static RunProperties copiedRunProperties(RunProperties existingProperties, boolean stripVerticalAlign, boolean stripRunStyle, boolean stripToggle) {
		if (existingProperties instanceof Empty) {
			return existingProperties;
		}

		List<RunProperty> newRunProperties = new ArrayList<>();

		for (RunProperty p : existingProperties.getProperties()) {
			// Ack!
			if (stripToggle && p instanceof RunProperty.WpmlToggleRunProperty) {
				continue;
			}
			if (stripRunStyle && p instanceof RunProperty.RunStyleProperty) {
				continue;
			}
			if (stripVerticalAlign && p instanceof RunProperty.GenericRunProperty) {
				if (SkippableElement.RunProperty.RUN_PROPERTY_VERTICAL_ALIGNMENT_WPML.toName().equals(p.getName())
					|| SkippableElement.RunProperty.RUN_PROPERTY_VERTICAL_ALIGNMENT_SSML.toName().equals(p.getName())) {
					continue; // skip it!
				}
			}
			newRunProperties.add(p);
		}

		return new Default(
			((Default) existingProperties).eventFactory,
			((Default) existingProperties).startElement,
			((Default) existingProperties).endElement,
			newRunProperties
		);
	}

	/**
	 * Creates copied run properties.
	 *
	 * @param runProperties Run properties
	 *
	 * @return Copied run properties
	 */
	static RunProperties copiedRunProperties(RunProperties runProperties) {
		if (runProperties instanceof Empty) {
			return runProperties;
		}

		List<RunProperty> properties = new ArrayList<>(runProperties.getProperties());

		return new Default(
			((Default) runProperties).eventFactory,
			((Default) runProperties).startElement,
			((Default) runProperties).endElement,
			properties
		);
	}

	/**
	 * Creates copied toggle run properties.
	 *
	 * @param runProperties Run properties
	 *
	 * @return Copied toggle run properties
	 */
	static RunProperties copiedToggleRunProperties(RunProperties runProperties) {
		if (runProperties instanceof Empty) {
			return runProperties;
		}

		List<RunProperty> properties = new ArrayList<>(runProperties.count());

		for (RunProperty property : runProperties.getProperties()) {
			if (property instanceof RunProperty.WpmlToggleRunProperty) {
				properties.add(property);
			}
		}

		return new Default(
			((Default) runProperties).eventFactory,
			((Default) runProperties).startElement,
			((Default) runProperties).endElement,
			properties
		);
	}

	/**
	 * Creates empty run properties.
	 *
	 * @return Empty run properties
	 */
	static RunProperties emptyRunProperties() {
		return new Empty();
	}

	/**
	 * Creates default run properties.
	 *
	 * @return Default run properties
	 */
	static RunProperties defaultRunProperties(
		final XMLEventFactory eventFactory,
		final StartElement startElement,
		final EndElement endElement,
		final RunProperty... properties
	) {
		return new Default(eventFactory, startElement, endElement, new ArrayList<>(Arrays.asList(properties)));
	}

	/**
	 * Checks whether current run properties are the subset of others.
	 *
	 * Empty run properties are not a subset of non-empty others.
	 *
	 * @param other Other run properties
	 *
	 * @return {@code true} - if current run properties are the subset of others
	 *         {@code false} - otherwise
	 */
	boolean isSubsetOf(RunProperties other) {
		if (getProperties().isEmpty() && !other.getProperties().isEmpty()) {
			return false;
		}

		// Algorithmically inefficient, but the number of properties in play is
		// generally so small that it should be fine.
outer:	for (RunProperty myProperty : getProperties()) {
			for (RunProperty otherProperty : other.getProperties()) {
				if (otherProperty.equalsProperty(myProperty)) {
					continue outer;
				}
			}
			return false;
		}
		return true;
	}

    /**
	 * Represents empty run properties.
	 *
	 * They are equal to the empty default run properties.
	 */
	static class Empty extends RunProperties {
		@Override
		public int hashCode() {
			return 1;
		}

		@Override
		protected boolean equalsProperties(RunProperties rp) {
			if (rp instanceof Default) {
				return this.count() == rp.count();
			}

			return (rp instanceof Empty);
		}

		@Override
		public List<XMLEvent> getEvents() {
			return Collections.emptyList();
		}

		@Override
		public List<RunProperty> getProperties() {
			return Collections.emptyList();
		}

		/**
		 * Combines current properties with other properties.
		 *
		 * @param otherProperties Other properties to match against
		 * @param traversalStage  The traversal stage
		 *
		 * @return Other run properties
		 */
		@Override
		public RunProperties combineDistinct(RunProperties otherProperties, StyleDefinitions.TraversalStage traversalStage) {
			return otherProperties;
		}

		@Override
		public String toString() {
			return "(No properties)";
		}
	}

	static class Default extends RunProperties implements Nameable {
		private final XMLEventFactory eventFactory;
		private final StartElement startElement;
		private final EndElement endElement;
		private final List<RunProperty> properties;

		Default(
			final XMLEventFactory eventFactory,
			final StartElement startElement,
			final EndElement endElement,
			final List<RunProperty> properties
		) {
			this.eventFactory = eventFactory;
			this.startElement = startElement;
			this.endElement = endElement;
			this.properties = properties;
		}

		@Override
		public List<RunProperty> getProperties() {
			return properties;
		}

		@Override
		public List<XMLEvent> getEvents() {
			final List<XMLEvent> events = new ArrayList<>();
			final List<RunProperty> attributeRunProperties = attributeRunProperties();
			final List<RunProperty> otherRunProperties = this.properties
				.stream()
				.filter(runProperty -> !attributeRunProperties.contains(runProperty))
				.collect(Collectors.toList());
			events.add(eventFactory.createStartElement(startElement.getName(), toAttributes(attributeRunProperties()).iterator(), startElement.getNamespaces()));
			for (RunProperty property : otherRunProperties) {
				events.addAll(property.getEvents());
			}
			events.add(endElement);
			return events;
		}

		private List<Attribute> toAttributes(List<RunProperty> properties) {
			return properties
				.stream()
				.map(property -> eventFactory.createAttribute(property.getName(), property.getValue()))
				.collect(Collectors.toList());
		}

		@Override
		public QName getName() {
			return this.startElement.getName();
		}

		/**
		 * Combines current properties with other properties.
		 *
		 * If a property is found in the list of others, it is replaced by the found one and the found one is removed.
		 * All non-matched other properties are added to the current list of properties.
		 *
		 * @param otherProperties Other properties to match against
		 * @param traversalStage  The traversal stage
		 *
		 * @return Current run properties
		 */
		@Override
		public RunProperties combineDistinct(RunProperties otherProperties, StyleDefinitions.TraversalStage traversalStage) {
			ListIterator<RunProperty> runPropertyIterator = properties.listIterator();

			while (runPropertyIterator.hasNext()) {
				RunProperty runProperty = runPropertyIterator.next();
				// cache start element name in order not to reconstruct it for some properties (e.g. FontsRunProperty)
				QName runPropertyStartElementName = runProperty.getName();

				Iterator<RunProperty> otherRunPropertyIterator = otherProperties.getProperties().iterator();

				while (otherRunPropertyIterator.hasNext()) {
					RunProperty otherRunProperty = otherRunPropertyIterator.next();

					if (runPropertyStartElementName.equals(otherRunProperty.getName())) {
						replace(runPropertyIterator, otherRunPropertyIterator, runProperty, otherRunProperty, traversalStage);
						break;
					}
				}

				if (otherProperties.getProperties().isEmpty()) {
					break;
				}
			}

			if (!otherProperties.getProperties().isEmpty()) {
				properties.addAll(otherProperties.getProperties());
			}

			return this;
		}

		private void replace(ListIterator<RunProperty> runPropertyIterator,
							 Iterator<RunProperty> otherRunPropertyIterator,
							 RunProperty runProperty,
							 RunProperty otherRunProperty,
							 StyleDefinitions.TraversalStage traversalStage) {

			if (runProperty instanceof RunProperty.WpmlToggleRunProperty) {

				if (StyleDefinitions.TraversalStage.VERTICAL == traversalStage) {
					boolean runPropertyValue = ((RunProperty.WpmlToggleRunProperty) runProperty).getToggleValue();
					boolean otherRunPropertyValue = ((RunProperty.WpmlToggleRunProperty) otherRunProperty).getToggleValue();

					if (!(runPropertyValue ^ otherRunPropertyValue)) {
						// exclusive OR resulted to "false", which means that the property can be removed, as it is the default value
						runPropertyIterator.remove();
						otherRunPropertyIterator.remove();
						return;
					}

					if (runPropertyValue) {
						// run property value is equal to "true" and other is "false", as the previous condition happens
						// only if both values are "true" or "false"
						otherRunPropertyIterator.remove();
						return;
					}

					// run property value is equal to "false" and other is "true",
					// move on to the default processing of all other types of properties
				}

				if (StyleDefinitions.TraversalStage.DOCUMENT_DEFAULT == traversalStage) {
					boolean runPropertyValue = ((RunProperty.WpmlToggleRunProperty) runProperty).getToggleValue();
					boolean otherRunPropertyValue = ((RunProperty.WpmlToggleRunProperty) otherRunProperty).getToggleValue();

					if (runPropertyValue && otherRunPropertyValue
							|| runPropertyValue) {
						// if run property value is equal to "true" and other run property value is equal to "whatever" value
						otherRunPropertyIterator.remove();
						return;
					}
					// run property value is equal to "false" and other is "true",
					// move on to the default processing of all other types of properties
				}

				// The MS Word does not follow up the flow of toggle properties processing and does substitute ANY value
				// in the styles hierarchy by ANY value if it has been specified later.
				// So, StyleDefinitions.TraversalStage.HORIZONTAL case is processed as all other types of properties.

				// StyleDefinitions.TraversalStage.DIRECT case is processed as all other types of properties
			}

			runPropertyIterator.set(otherRunProperty);
			otherRunPropertyIterator.remove();
		}

		@Override
		protected boolean equalsProperties(RunProperties o) {
			if (o instanceof Empty) {
				return this.count() == o.count();
			}
			if (!(o instanceof Default)) return false;
			// Compare start events - this ensures the element/namespace is the same,
			// and also will compare attributes in the DrawingML case.
			Default rp = (Default)o;
			if (!startElementEquals(startElement, rp.startElement)) {
				return false;
			}
			// TODO handle out of order properties
			return properties.equals(rp.properties);
		}

		@Override
		public int hashCode() {
			return Objects.hash(startElement, endElement, properties);
		}

		@Override
		public String toString() {
			return "rPr(" + properties.size() + ")[" + properties + "]";
		}
	}
}
