package net.sf.okapi.virtualdb;

import java.io.File;

import net.sf.okapi.virtualdb.jdbc.Repository;
import net.sf.okapi.virtualdb.jdbc.h2.H2Access;

public class SelfCleaningRepo extends Repository implements AutoCloseable {
	private final File fullPath;

	public SelfCleaningRepo(String outputDir, H2Access acc) {
		super(acc);
		this.create("myRepo");
		fullPath = new File(outputDir, "myRepo.mv.db");
	}

	@Override
	public void close() {
		super.close();
		fullPath.delete();
	}
}
