package net.sf.okapi.lib.xliff2.processor;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.StringWriter;

import net.sf.okapi.lib.xliff2.InvalidParameterException;
import net.sf.okapi.lib.xliff2.core.Fragment;
import net.sf.okapi.lib.xliff2.core.Unit;
import net.sf.okapi.lib.xliff2.document.XLIFFDocument;
import net.sf.okapi.lib.xliff2.processor.DefaultEventHandler;
import net.sf.okapi.lib.xliff2.processor.XLIFFProcessor;
import net.sf.okapi.lib.xliff2.reader.Event;
import net.sf.okapi.lib.xliff2.reader.XLIFFReader;
import net.sf.okapi.lib.xliff2.test.U;

import org.junit.Test;

public class XLIFFProcessorTest {

	private final String root = U.getParentDir(this, "/example.xlf")+"/";

	class SetCanResegment extends DefaultEventHandler {
		@Override
		public Event handleUnit (Event event) {
			Unit unit = event.getUnit();
			// Set canResegment to false on the unit
			unit.setCanResegment(false);
			return event;
		}
	}
	
	class AddText extends DefaultEventHandler {
		@Override
		public Event handleUnit (Event event) {
			Unit unit = event.getUnit();
			Fragment fragment = unit.getPart(unit.getPartCount()-1).getSource();
			fragment.setCodedText(fragment.getCodedText()+"_NEW");
			return event;
		}
	}
	
	@Test (expected = InvalidParameterException.class)
	public void testSameInputOutput () {
		XLIFFProcessor processor = new XLIFFProcessor();
		processor.run(new File(root+"example.out1.xlf"), new File(root+"example.out1.xlf"));
	}

	@Test
	public void testNoOutput () {
		// Create the processor and the event handler
		XLIFFProcessor processor = new XLIFFProcessor();
		processor.setHandler(new SetCanResegment());
		// Process the file
		File inputFile = new File(root+"example.xlf");
		processor.run(inputFile, null);
		// No error should occur with a null output
		// Do it again, this time after setting the output differently
		processor.setInput(inputFile);
		processor.setOutput((StringWriter)null);
		processor.run();
	}
	
	@Test
	public void testSingleHandler () {
		// Create the processor and the event handler
		XLIFFProcessor processor = new XLIFFProcessor();
		processor.setHandler(new SetCanResegment());
		// make sure any existing output file is deleted
		File outFile = new File(root+"example.out1.xlf");
		outFile.delete();
		// Perform the modification
		processor.run(new File(root+"example.xlf"), outFile);
		// Check the expected output
		XLIFFDocument doc = new XLIFFDocument();
		doc.load(outFile, XLIFFReader.VALIDATION_MAXIMAL);
		Unit unit = doc.getUnitNode("f1", "1").get();
		assertEquals(false, unit.getCanResegment());
	}
	
	@Test
	public void testTwoHandlers () {
		// Create the processor and the event handler
		XLIFFProcessor processor = new XLIFFProcessor();
		processor.setHandler(new SetCanResegment());
		processor.add(new AddText());
		// make sure any existing output file is deleted
		File outFile = new File(root+"example.out2.xlf");
		outFile.delete();
		// Perform the modification
		processor.run(new File(root+"example.xlf"), outFile);
		// Check the expected output
		XLIFFDocument doc = new XLIFFDocument();
		doc.load(outFile, XLIFFReader.VALIDATION_MAXIMAL);
		Unit unit = doc.getUnitNode("f1", "1").get();
		assertEquals(false, unit.getCanResegment());
	}
	
}
